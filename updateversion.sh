#!sh
# Generates the *_rev.h header, and updates the version
#
# Author: Hans de Ruiter
# Copyright (C) 2020 by Kea Sigma Delta Limited, all rights reserved
#
# Version 0.1 - 2019/08/19 - Initial version

# For OS-specific stuff...
os_name="$(uname -s)"

scriptname=$(basename $0)

if [ "$#" -lt 1 ]; then
	echo "$scriptname: Updates the version file header, optionally bumping the version or 
revision, and updating the build number

Usage:
$scriptname <basename> [options]
This will generate <basename>_rev.h, and create or update <basename>_rev.rev and 
<basename_rev.vstring as needed. The version string will be of the form:
version.revision build-number (date) <DEBUG>
where:
- build-number is either the $CI_JOB_ID environment variable for 
  Constant Integration (CI) builds, or a timestamp in 
  YYYMMDD.HHMMSS-dev format (e.g., 20200423.115023-dev)
- <DEBUG> will be appended for builds with DEBUG defined and not NDEBUG

Options:
--bumpver - increments the major version number. This will also reset the 
            revision to 0
--bumprev - increments the revision number
--setver <n> - sets the version number to <n>
--setrev <n> - sets the revsion number to <n>
--name <progname> - the next parameter is the program name (defaults to <basename>)

IMPORTANT: Make sure you add the *_rev.rev. It's recommended to NOT put the header 
file or *.vstring file in, because you will likely end up with git pull failing due 
to conflicts. Instead, add the version string to a commit message ot tag-name (you 
can read it from the *_rev.vstring file)

Setting up the makefile:
You'll need an \"incver\" rule that is executed every time. For example, if you
have a makefile with $(TARGET) giving the name of the program, then:
$(TARGET): incver <insert other dependencies here>

.PHONY: incver
incver:
	updateversion $(TARGET)

Using the version header:
The version header has the following defined:
- VERSION - the major version
- REVISION - the revision
- BUILDID - the build-number as a string
- DATE - the date as a string in the following format \"dd.mm.yyyy\" (e.g., 
         \"23.4.2020\"
- VSHORT - A very short version string: \"<progname> VERSION.REVISION\"
- VERS - A short version string: \"<progname> VERSION.REVISION BUILDID\"
- VSTRING - The full version string: 
            \"<progname> VERSION.REVISION (dd.mm.yyyy) BUILDID\"
- VERSTAG - An AmigaOS style version tag (just like VSTRING, but with
            \\0$VER: prepended"
fi


# Function to check if a command line option's parameter is set
# Pass this function the option name and the parameter variable as
# input (e.g., check_params "$1" "$2")
check_params() {
	if [ -z "$2" ]; then
		echo "ERROR: Command line option $1 is missing its parameter"
		exit
	fi
}

# parse args

PARAMS=""
while (( "$#" )); do
	case "$1" in
		--bumpver)
			bump_ver=1
			shift
			;;
		--bumprev)
			bump_rev=1
			shift
			;;
		--setver)
			set_ver=$2
			check_params "$1" "$2"
			shift 2
			;;
		--setrev)
			set_rev=$2
			check_params "$1" "$2"
			shift 2
			;;
		--name)
			progname=$2
			check_params "$1" "$2"
			shift 2
			;;
		--) # end argument parsing
			shift
			break
			;;
		-*|--*=) # unsupported flags
			echo "Error: Unsupported flag $1" >&2
			exit 1
			;;
		*) # preserve positional arguments
			PARAMS="$PARAMS $1"
			shift
			;;
	esac
done 

# Set positional arguments in their proper place
eval set -- "$PARAMS"

basename="$1"
if [ -z "$progname" ]; then
	progname=$basename
fi
header_file="${basename}_rev.h"
rev_file="${basename}_rev.rev"
vstring_file="${basename}_rev.vstring"




# Extract the version information from the header file (or create it if it 
# doesn't exist)
version="" # Some systems have a version environment var, which can mess this up...
if [ -f "$rev_file" ]; then
	version=$(grep -a -o "VERSION.*" ${rev_file} | awk '{$1= ""; print $0}')
	version=${version%$'\r'} # Echo generates \r\n on some systems, and we need to filter it out...
	revision=$(grep -a -o "REVISION.*" ${rev_file} | awk '{$1= ""; print $0}')
	revision=${revision%$'\r'} # Echo generates \r\n on some systems, and we need to filter it out...
	if [ -z "$version" ] || [ -z "$revision" ]; then
		echo "ERROR: $rev_file is invalid (missing version and/or revision)"
		exit 10
	fi
	if ! [ "$version" -eq "$version" 2>/dev/null ] || ! [ "$revision" -eq "$revision" 2>/dev/null ]; then
		echo "ERROR: $rev_file is invalid (version and revision must be numeric)"
		exit 10
	fi
	version=$(($version + 0)) # Making numeric (& stripping whitespace)
	revision=$(($revision + 0)) # Making numeric (& stripping whitespace)
else
	# New file
	echo "No $rev_file found. Creating it..."
	version=0
	revision=1
fi


# Apply any version & revision changes
# NOTE: These are in a specific order, with most important rules last (so they
# override less important rules
if [ -n "$bump_rev" ]; then
	revision=$(($revision + 1))
fi
if [ -n "$bump_ver" ]; then
	version=$(($version + 1))
	revision=0
fi


if [ -n "$set_rev" ]; then
	revision=$set_rev
fi
if [ -n "$set_ver" ]; then
	version=$set_ver
fi


# Generate the timestamps
timestamp_fmt="+%Y%m%d.%H%M%S"
date_fmt="+%d.%m.%Y"
case "${os_name}" in
	AmigaOS*)
		release_timestamp=$(gdate $timestamp_fmt)
		release_date=$(gdate $date_fmt)
		;;
	*)
		release_timestamp=$(date $timestamp_fmt)
		release_date=$(date $date_fmt)
		;;
esac


# Generate the build_number
if [ ! -z "${CI_JOB_ID}" ]; then
	# An official CI/CD build
	echo "CI/CD build $CI_JOB_ID"
	build_number="$CI_JOB_ID"
else 
	# Internal developer build
	echo "Internal developer build"
	build_number="${release_timestamp}-dev"
fi


# Workaround for "\\0" behaving differently on AmigaOS
case "${os_name}" in
	AmigaOS*) bs='\\' ;;
	*) bs='\';;
esac


# Generate the *.rev file
echo "// Automatically generated by $scriptname. Do not edit" > $rev_file
echo "VERSION   $version" >> $rev_file
echo "REVISION  $revision" >> $rev_file

# Generate the revision header
echo "// Automatically generated by $scriptname. Do not edit" > $header_file
echo "#define VERSION   $version" >> $header_file
echo "#define REVISION  $revision" >> $header_file
echo "#define BUILDID   \"$build_number\"" >> $header_file
echo "#define DATE      \"$release_date\"" >> $header_file
echo "#define VSHORT    \"$progname $version.$revision\"" >> $header_file
echo "#define VERS      \"$progname $version.$revision $build_number\"" >> $header_file
vstring="$progname $version.$revision ($release_date) $build_number"
echo "#define VSTRING   \"$vstring\"" >> $header_file
echo "#define VERSTAG   \"${bs}0\$VER: $progname $version.$revision ($release_date) $build_number\"" >> $header_file

# Generate the *.vstring file
echo "$vstring" > $vstring_file

# Let the user know what version was output

echo "Version of $progname updated to: $version.$revision ($release_date) $build_number"
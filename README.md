# PCICapList

A tool to list PCI device capabilities (for AmigaOS 4).

Copyright (C) 2021 by Kea Sigma Delta Limited, all rights reserved.

Author: Hans de Ruiter

## Usage

Run the program giving the PCI vendor and device IDs of the device whose capabilities registers you wish to read. For example:
PCICapList 0x1002 0x67DF

## License

See the accompanying LICENSE file.

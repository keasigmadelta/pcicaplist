# Makefile for the PCICapList
#
# Author: Hans de Ruiter
#
# Copyright (c) 2021 by Kea Sigma Delta Limited, all rights reserved

TARGET = PCICapList

CC = ppc-amigaos-gcc
STRIP = ppc-amigaos-strip 

CFLAGS = -Wall -mcrt=newlib -gstabs -MMD -MP

SRC=PCICapList.c PrintPCICapabilities.c
OBJ=$(patsubst %.c, %.o, ${SRC}) 
DEP=$(patsubst %.c, %.d, ${SRC}) 

.PHONY: all
all: 	$(TARGET) 

$(TARGET): incver $(OBJ)
	$(CC) $(LDFLAGS) -o $@.debug $(OBJ) $(LDLIBS)
	$(STRIP) $@.debug -o $@

%.o: %.c | incver
	$(CC) -c $(CFLAGS) -o $@ $<

.PHONY: clean
clean:
		rm $(OBJ) $(DEP) $(TARGET) $(TARGET).debug

# Generate a unique version number every time
.PHONY: incver
incver:
	updateversion.sh "$(TARGET)"

.PHONY: revision
revision:
	updateversion.sh --bumprev "$(TARGET)"

.PHONY: version
version:
	updateversion.sh --bumpver "$(TARGET)"

-include $(DEP)
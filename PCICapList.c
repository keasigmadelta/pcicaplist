/** Gets a list of PCI capabilities for the specified device.
 *
 * @Copyright (C) 2021 by Kea Signa Delta Limited, all rights reserved.
 *
 * See LICENSE file for licensing information.
 */

#include <exec/exec.h>
#include <exec/types.h>
#include <expansion/expansion.h>
#include <expansion/pci.h>

#include <proto/exec.h>
#include <proto/expansion.h>

#include <stdio.h>
#include <stdlib.h>

#include "PCIRegs.h"
#include "PCICapList_rev.h"
#include "PrintPCICapabilities.h"


STATIC CONST UBYTE USED verstag[] = VERSTAG;

#define MIN_EXPANSION_LIB_VER 50

// Minimum exec.library version for the PCI capabilities API
#define PCICAPAPI_MIN_EXEC_VERSION 53
#define PCICAPAPI_MIN_EXEC_REVISION 61

struct Library *ExpansionBase = NULL;
struct PCIIFace *IPCI = NULL;



BOOL openLibs() {
	if(!LIB_IS_AT_LEAST((struct Library *)SysBase, PCICAPAPI_MIN_EXEC_VERSION, PCICAPAPI_MIN_EXEC_REVISION)) {
		printf("ERROR: Need at least exec.library version %u.%u to read PCI capability registers\n", 
			PCICAPAPI_MIN_EXEC_VERSION, PCICAPAPI_MIN_EXEC_REVISION);
		return FALSE;
	}

	ExpansionBase = IExec->OpenLibrary("expansion.library", MIN_EXPANSION_LIB_VER);
	if(!ExpansionBase) {
		printf("ERROR: Unable to open expansion.library version %d.\n", MIN_EXPANSION_LIB_VER);
		return FALSE;
	}

	IPCI = (struct PCIIFace*)IExec->GetInterface((struct Library*)ExpansionBase, "pci", 1, NULL);
	if(!IPCI) {
		printf("ERROR: Could not open expansion.library's PCI interface\n");
		return FALSE;
	}
	
	return TRUE;
}

void cleanup() {
	if(IPCI) {
		IExec->DropInterface((struct Interface*)IPCI);
		IPCI = NULL;
	}

	if(ExpansionBase)
	{
 		IExec->CloseLibrary((struct Library*)ExpansionBase);
		ExpansionBase = NULL;
	}
}

void printHelp(const char *name) {
	printf("%s: Lists a PCI(e) device's capabilities\n", name);
	printf("Usage:\n");	
	printf("%s <PCI Vendor ID> <PCI Device ID>\n", name);
	printf("Both values should be in hex format, e.g., 0x1002\n");
	printf("NOTE: You can use a tool like Ranger to get a device's IDs\n");
}

int main(int argc, const char **argv) {
	atexit(cleanup);
	if(!openLibs()) {
		printf("Couldn't open needed libraries. Exiting\n");
		return 10;
	}

	if(argc != 3) {
		printHelp(argv[0]);
		return 0;
	}

	uint16 vendorID = (uint16)strtoul(argv[1], NULL, 16);
	uint16 deviceID = (uint16)strtoul(argv[2], NULL, 16);

	printf("Searching for a PCI(e) device with VendorID: 0x%X, and deviceID: 0x%X\n", (unsigned)vendorID, (unsigned)deviceID);

	struct PCIDevice *pciDev = IPCI->FindDeviceTags(FDT_VendorID, vendorID,
    							FDT_DeviceID, deviceID,
    							TAG_DONE);
	if(!pciDev) {
		printf("ERROR: No PCI(e) device found with the specified IDs. Exiting.\n");
		return 10;
	}

	printf("PCI(e) device found\n");
	printf("\n");
	printf("Capabilities:\n");
	for(struct PCICapability *curr = pciDev->GetFirstCapability(); curr != NULL; 
			curr = pciDev->GetNextCapability(curr)) {
		const char *detailPrefix = "\t    \t    ";
		printf("\t0x%02X\t", (unsigned)curr->Type);
		switch(curr->Type) {
		case PCI_CAPABILITYID_PM:
			printf("Power Management\n");
			break;
		case PCI_CAPABILITYID_AGP:
			printf("Accelerated Graphics Port\n");
			break;
		case PCI_CAPABILITYID_VPD:
			printf("Vital Product Data\n");
			break;
		case PCI_CAPABILITYID_SLOTID:
			printf("Slot identification\n");
			break;
		case PCI_CAPABILITYID_MSI:
			printf("Message Signaled Interrupts\n");
			break;
		case PCI_CAPABILITYID_CHSWP:
			printf("Compact PCI hotswap\n");
			break;
		case PCI_CAPABILITYID_PCIX:
			printf("PCI-X\n");
			break;
		case PCI_CAPABILITYID_AMD:
			printf("Reserved for AMD\n");
			break;
		case PCI_CAPABILITYID_VENDOR:
			printf("Vendor specific\n");
			break;
		case PCI_CAPABILITYID_DEBUG:
			printf("Debug port\n");
			break;
		case PCI_CAPABILITYID_CPRSC:
			printf("Compact PCI central resource control\n");
			break;
		case PCI_CAPABILITYID_SHPC:
			printf("Standard Hotplug Controller\n");
			break;
		case PCI_CAPABILITYID_BRIDGE:
			printf("Bridge subsystem vendor/device ID\n");
			break;
		case PCI_CAPABILITYID_AGP3:
			printf("AGP PCI-PCI bridge\n");
			break;
		case PCI_CAPABILITYID_SECDEV:
			printf("Secure device\n");
			break;
		case PCI_CAPABILITYID_EXP:
			printf("PCI Express\n");
			printPCIExpressCapRegs(pciDev, curr, detailPrefix);
			break;
		case PCI_CAPABILITYID_MSIX:
			printf("MSI-X\n");
			break;
		case PCI_CAPABILITYID_SATA:
			printf("SATA\n");
			break;
		case PCI_CAPABILITYID_AF:
			printf("Advanced feature\n");
			break;
		default:
			printf("???\n");
		}
	}
	
	printf("\n");
	// ##### FIXME! ##### Should skip extended capability listing if config size is < PCI_CONFIG_SIZE.
	// ##### FIXME! ##### Can't find a size field in PCI config space, though...
	uint32 pciCapPos = PCI_CONFIG_SPACE_SIZE;
	uint32 pciCapHeader = pciDev->ReadConfigLong(pciCapPos);
	if(pciCapHeader == 0) {
		printf("PCI(e) device has no extended capability extensions\n");
	} else {
		printf("Extended Capabilities:\n");
		do {
			pciCapHeader = pciDev->ReadConfigLong(pciCapPos);

			uint8 type = pciCapHeader & 0xFFFF;
		
			printf("\t0x%02X\t", type);
			switch(type) {
			case PCI_EXT_CAPABILITYID_ERR:
				printf("Advanced error reporting\n");
				break;
			case PCI_EXT_CAPABILITYID_VC:
				printf("Virtual channel\n");
				break;
			case PCI_EXT_CAPABILITYID_DSN:
				printf("Device serial number\n");
				break;
			case PCI_EXT_CAPABILITYID_PWR:
				printf("Power budgeting\n");
				break;
			case PCI_EXT_CAPABILITYID_RCLD:
				printf("Root complex link declaration\n");
				break;
			case PCI_EXT_CAPABILITYID_RCILC:
				printf("Root complex internal link control\n");
				break;
			case PCI_EXT_CAPABILITYID_RCEC:
				printf("Root complex event collector\n");
				break;
			case PCI_EXT_CAPABILITYID_MFVC:
				printf("Multi-function VC\n");
				break;
			case PCI_EXT_CAPABILITYID_VC9:
				printf("Virtual channel (alt. version)\n");
				break;
			case PCI_EXT_CAPABILITYID_RCRB:
				printf("Root complex RB?\n");
				break;
			case PCI_EXT_CAPABILITYID_VENDOR:
				printf("Vendor specific\n");
				break;
			case PCI_EXT_CAPABILITYID_CAC:
				printf("Config access (obsolete)\n");
				break;
			case PCI_EXT_CAPABILITYID_ACS:
				printf("Access control services\n");
				break;
			case PCI_EXT_CAPABILITYID_ARI:
				printf("Alternative routing ID\n");
				break;
			case PCI_EXT_CAPABILITYID_ATS:
				printf("Address translation services\n");
				break;
			case PCI_EXT_CAPABILITYID_SRIOV:
				printf("Single root I/O virtualization\n");
				break;
			case PCI_EXT_CAPABILITYID_MRIOV:
				printf("Multi root I/O virtualization\n");
				break;
			case PCI_EXT_CAPABILITYID_MCAST:
				printf("Multicast\n");
				break;
			case PCI_EXT_CAPABILITYID_PRI:
				printf("Page request interface\n");
				break;
			case PCI_EXT_CAPABILITYID_AMD_XXX:
				printf("Reserved for AMD\n");
				break;
			case PCI_EXT_CAPABILITYID_REBAR:
				printf("Resizable BAR\n");
				break;
			case PCI_EXT_CAPABILITYID_DPA:
				printf("Dynamic power allocation\n");
				break;
			case PCI_EXT_CAPABILITYID_TPH:
				printf("TPH requester\n");
				break;
			case PCI_EXT_CAPABILITYID_LTR:
				printf("Latency tolerance reporting\n");
				break;
			case PCI_EXT_CAPABILITYID_SECPCI:
				printf("Secondary PCIe capability\n");
				break;
			case PCI_EXT_CAPABILITYID_PMUX:
				printf("Protocol multiplexing\n");
				break;
			case PCI_EXT_CAPABILITYID_PASID:
				printf("Process address space ID\n");
				break;
			case PCI_EXT_CAPABILITYID_DPC:
				printf("Downstream port containment\n");
				break;
			case PCI_EXT_CAPABILITYID_L1SS:
				printf("L1 PM substates\n");
				break;
			case PCI_EXT_CAPABILITYID_PTM:
				printf("Precision time measurement\n");
				break;
			case PCI_EXT_CAPABILITYID_DLF:
				printf("Data link feature\n");
				break;
			case PCI_EXT_CAPABILITYID_PL_16GT:
				printf("Physical layer 16.0 GT/s\n");
				break;

			default:
				printf("???\n");
			}
			
			// Get the next capability
			pciCapPos = (pciCapHeader >> 20) & 0xFFC;
		} while(pciCapPos > PCI_CONFIG_SPACE_SIZE && pciCapPos < PCI_CONFIG_SPACE_EXT_SIZE);
	}

	IPCI->FreeDevice(pciDev);
	pciDev = NULL;
	
	return 0;
}
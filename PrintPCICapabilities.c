/** PrintPCICapabilities.c
 *
 * See header file for details.
 */

#include "PrintPCICapabilities.h"

#include <stdio.h>

#include "PCIRegs.h"


/** Returns the PCIe device's type as a string.
 *
 * @param pcieFlags the PCIE_EXP_FLAGS register
 */
static const char* getPCIEDeviceType(uint16 pcieFlags) {
	uint16 type = PCI_EXP_FLAGS_TYPE_G(pcieFlags);
	switch(type) {
	case PCI_EXP_TYPE_PCIE_ENDPOINT:
		return "Endpoint";
	case PCI_EXP_TYPE_LEGACY_ENDPOINT:
		return "Legacy endpoint";
	case PCI_EXP_TYPE_ROOT_PORT:
		return "Root port";
	case PCI_EXP_TYPE_UPSTREAM_PORT:
		return "Upstream port";
	case PCI_EXP_TYPE_DOWNSTRAM_PORT:
		return "Downstream port";
	case PCI_EXP_TYPE_PCI_BRIDGE:
		return "PCI/PCI-X bridge";
	case PCI_EXP_TYPE_ROOT_COMPLEX_INTEGRATED_ENDPOINT:
		return "Root complex integrated endpoint";
	case PCI_EXP_TYPE_ROOT_COMPLEX_EVENT_COLLECTOR:
		return "Root complex event collector";
	default:
		return "???";
	}
}

/** Gets the link speed as text.
 */
static const char* getLinkSpeedString(unsigned linkSpeed) {
	switch(linkSpeed) {
	case PCI_EXP_LINKCAP_SLS_UNKNOWN:
	default:
		return "Unknown\n";
	case PCI_EXP_LINKCAP_SLS_2_5GT:
		return "2.5GT/s\n";
	case PCI_EXP_LINKCAP_SLS_5_0GT:
		return "5.0GT/s\n";
	case PCI_EXP_LINKCAP_SLS_8_0GT:
		return "8.0GT/s\n";
	case PCI_EXP_LINKCAP_SLS_16_0GT:
		return "16.0GT/s\n";
	case PCI_EXP_LINKCAP_SLS_32_0GT:
		return "32.0GT/s\n";
	case PCI_EXP_LINKCAP_SLS_64_0GT:
		return "64.0GT/s\n";
	}
}

void printPCIExpressCapRegs(struct PCIDevice *pciDevice, struct PCICapability *pcieCap, const char *prefix) {
	// Flags register
	uint16 pcieFlags = pciDevice->ReadConfigWord(pcieCap->CapOffset + PCI_EXP_FLAGS_REGOFFSET);
	printf("%sPCIe flags: 0x%04X\n", prefix, pcieFlags);
	const char * type = getPCIEDeviceType(pcieFlags);
	printf("%s    Type: %s\n", prefix, type);
	if(PCI_EXP_FLAGS_SLOT_IMPLEMENTED_G(pcieFlags)) {
		printf("%s    Slot implemented\n", prefix);
	}
	uint16 irqMessageNum = PCI_EXP_FLAGS_IRQ_MESSAGE_NUMBER_G(pcieFlags);
	printf("%s    IRQ message number: %u\n", prefix, (unsigned)irqMessageNum);

	// Device capabilities
	uint32 pcieDevCap = pciDevice->ReadConfigLong(pcieCap->CapOffset + PCI_EXP_DEVCAP);
	printf("%sPCIe device capabilities: 0x%08X\n", prefix, (unsigned)pcieDevCap);
	printf("%s    Maximum payload size: %u bytes\n", prefix, 
		(unsigned)(128 << PCI_EXP_DEVCAP_MAX_PAYLOAD_SIZE_G(pcieDevCap)));
	printf("%s    Phantom functions: 0x%X\n", prefix, (unsigned)PCI_EXP_DEVCAP_PHANTOM_FUNCS_G(pcieDevCap));
	if(PCI_EXP_DEVCAP_PHANTOM_FUNCS_G(pcieDevCap)) {
		printf("%s    Extended tags\n", prefix);
	}
	printf("%s    Acceptable L0s latency: %u\n", prefix, (unsigned)PCI_EXP_DEVCAP_L0S_LATENCY_G(pcieDevCap));
	printf("%s    Acceptable L1 latency: %u\n", prefix, (unsigned)PCI_EXP_DEVCAP_L1_LATENCY_G(pcieDevCap));
	if(PCI_EXP_DEVCAP_ATTENTION_BUTTON_G(pcieDevCap)) {
		printf("%s    Attention button present\n", prefix);
	}
	if(PCI_EXP_DEVCAP_ATTENTION_INDICATOR_G(pcieDevCap)) {
		printf("%s    Attention indicator present\n", prefix);
	}
	if(PCI_EXP_DEVCAP_POWER_INDICATOR_G(pcieDevCap)) {
		printf("%s    Power indicator present\n", prefix);
	}
	if(PCI_EXP_DEVCAP_ROLE_BASED_ERROR_REPORTING_G(pcieDevCap)) {
		printf("%s    Role based error reporting\n", prefix);
	}
	printf("%s    Slot power limit value: %u\n", prefix, (unsigned)PCI_EXP_DEVCAP_POWER_VALUE_G(pcieDevCap));
	printf("%s    Slot power limit scale: %u\n", prefix, (unsigned)PCI_EXP_DEVCAP_POWER_SCALE_G(pcieDevCap));
	if(PCI_EXP_DEVCAP_FUNCTION_LEVEL_RESET_G(pcieDevCap)) {
		printf("%s    Function level reset\n", prefix);
	}

	// Device control
	uint32 pcieDevCtl = pciDevice->ReadConfigWord(pcieCap->CapOffset + PCI_EXP_DEVCTL);
	printf("%sPCIe device control: 0x%04X\n", prefix, (unsigned)pcieDevCtl);
	if(PCI_EXP_DEVCTL_CER_EN_G(pcieDevCtl)) {
		printf("%s    Correctable error reporting enabled\n", prefix);
	}
	if(PCI_EXP_DEVCTL_NFER_EN_G(pcieDevCtl)) {
		printf("%s    Non-fatal error reporting enabled\n", prefix);
	}
	if(PCI_EXP_DEVCTL_URR_EN_G(pcieDevCtl)) {
		printf("%s    Unsupported request reporting enabled\n", prefix);
	}
	if(PCI_EXP_DEVCTL_RELAXED_ORDERING_EN_G(pcieDevCtl)) {
		printf("%s    Relaxed ordering enabled\n", prefix);
	}
	printf("%s    Max payload size: %u\n", prefix,
		(unsigned)(128 << PCI_EXP_DEVCTL_MAX_PAYLOAD_SIZE_G(pcieDevCtl)));
	printf("%s    Max read request size: %u\n", prefix,
		(unsigned)(128 << PCI_EXP_DEVCTL_MAX_READ_REQ_SIZE_G(pcieDevCtl)));
	if(PCI_EXP_DEVCTL_EXTENDED_TAG_EN_G(pcieDevCtl)) {
		printf("%s    Extended tag enabled\n", prefix);
	}
	if(PCI_EXP_DEVCTL_PHANTOM_EN_G(pcieDevCtl)) {
		printf("%s    Phantom functions enabled\n", prefix);
	}
	if(PCI_EXP_DEVCTL_AUX_PM_EN_G(pcieDevCtl)) {
		printf("%s    Auxiliary power PM enabled\n", prefix);
	}
	if(PCI_EXP_DEVCTL_NOSNOOP_EN_G(pcieDevCtl)) {
		printf("%s    No snoop enabled\n", prefix);
	}
	if(PCI_EXP_DEVCTL_BRIDGE_CONF_RETRY_FLR_G(pcieDevCtl)) {
		printf("%s    Bridge configuration retry/flr \n", prefix);
	}


	// Device Status
	uint32 pcieDevStatus = pciDevice->ReadConfigWord(pcieCap->CapOffset + PCI_EXP_DEVSTATUS);
	printf("%sPCIe device status: 0x%04X\n", prefix, (unsigned)pcieDevStatus);
	if(PCI_EXP_DEVSTA_CORRECTABLE_ERROR_DETECTED_G(pcieDevStatus)) {
		printf("%s    Correctable error detected \n", prefix);
	}
	if(PCI_EXP_DEVSTA_NONFATAL_ERROR_DEECTED_G(pcieDevStatus)) {
		printf("%s    Non-fatal error detected \n", prefix);
	}
	if(PCI_EXP_DEVSTA_FATAL_ERROR_DETECTED_G(pcieDevStatus)) {
		printf("%s    Fatal error detected \n", prefix);
	}
	if(PCI_EXP_DEVSTA_UNSUPPORTED_REQ_DETECTED_G(pcieDevStatus)) {
		printf("%s    Unsupported request detected \n", prefix);
	}
	if(PCI_EXP_DEVSTA_AUX_POWER_DETECTED_G(pcieDevStatus)) {
		printf("%s    Auxiliary power detected \n", prefix);
	}
	if(PCI_EXP_DEVSTA_TRANSACTIONS_PENDING_G(pcieDevStatus)) {
		printf("%s    Transactions pending \n", prefix);
	}

	// Link Capabilities
	uint32 pcieLinkCap = pciDevice->ReadConfigLong(pcieCap->CapOffset + PCI_EXP_LINKCAP);
	printf("%sPCIe link capabilities: 0x%08X\n", prefix, (unsigned)pcieLinkCap);
	unsigned supportedLinkSpeed = PCI_EXP_LNKCAP_SUPPORTED_LINK_SPEEDS_G(pcieLinkCap);
	const char *supportedLinkSpeedStr = getLinkSpeedString(supportedLinkSpeed);
	printf("%s    Max. supported link speed: %s", prefix, supportedLinkSpeedStr);
	printf("%s    Max link width: %u\n", prefix, (unsigned)PCI_EXP_LNKCAP_MAX_LINK_WIDTH_G(pcieLinkCap));
	printf("%s    Active State Power Management: %u\n", prefix, (unsigned)PCI_EXP_LNKCAP_ASPMS_G(pcieLinkCap));
	printf("%s    L0s exit latency: %u\n", prefix, (unsigned)PCI_EXP_LNKCAP_L0S_EXIT_LATENCY_G(pcieLinkCap));
	printf("%s    L1 exit latency: %u\n", prefix, (unsigned)PCI_EXP_LNKCAP_L1_EXIT_LATENCY_G(pcieLinkCap));
	if(PCI_EXP_LNKCAP_L1_CLOCK_PM_G(pcieLinkCap)) {
		printf("%s    L1 clock power management\n", prefix);
	}
	if(PCI_EXP_LNKCAP_SDERC_G(pcieLinkCap)) {
		printf("%s    Surprise down error reporting capable\n", prefix);
	}
	if(PCI_EXP_LNKCAP_DLLLARC_G(pcieLinkCap)) {
		printf("%s    Data link layer link active reporting capable\n", prefix);
	}
	if(PCI_EXP_LNKCAP_LBNC_G(pcieLinkCap)) {
		printf("%s    Link bandwidth notification capability\n", prefix);
	}
	if(PCI_EXP_LNKCAP_BIT22_G(pcieLinkCap)) {
		printf("%s    Bit 22 set (unknown meaning)\n", prefix);
	}
	if(PCI_EXP_LNKCAP_BIT23_G(pcieLinkCap)) {
		printf("%s    Bit 23 set (unknown meaning)\n", prefix);
	}
	printf("%s    Port number: %u\n", prefix, (unsigned)PCI_EXP_LNKCAP_PORT_NUMBER_G(pcieLinkCap));

	
	// Link control
	uint32 pcieLinkControl = pciDevice->ReadConfigWord(pcieCap->CapOffset + PCI_EXP_LINKCTL);
	printf("%sPCIe link control: 0x%04X\n", prefix, (unsigned)pcieLinkControl);
	printf("%s    Active State Power Management: %u\n", prefix, 
		(unsigned)PCI_EXP_LINKCTL_ASPM_CONTROL_G(pcieLinkControl));
	if(PCI_EXP_LINKCTL_READ_COMPLETION_BOUNDARY_G(pcieLinkControl)) {
		printf("%s    Read completion boundary set\n", prefix);
	}
	if(PCI_EXP_LINKCTL_LINK_DISABLE_G(pcieLinkControl)) {
		printf("%s    Link disable\n", prefix);
	}
	if(PCI_EXP_LINKCTL_RETRAIN_LINK_G(pcieLinkControl)) {
		printf("%s    Retrain link set\n", prefix);
	}
	if(PCI_EXP_LINKCTL_COMMON_CLOCK_CONFIG_G(pcieLinkControl)) {
		printf("%s    Common clock configuration set\n", prefix);
	}
	if(PCI_EXP_LINKCTL_EXTENDED_SYNC_G(pcieLinkControl)) {
		printf("%s    Extended sync. set\n", prefix);
	}
	if(PCI_EXP_LINKCTL_EN_CLKREQ_G(pcieLinkControl)) {
		printf("%s    Enable clockreq\n", prefix);
	}
	if(PCI_EXP_LINKCTL_HW_AUTONOMOUS_WIDTH_DISABLE_G(pcieLinkControl)) {
		printf("%s    Hardware autonomous width disabled\n", prefix);
	}
	if(PCI_EXP_LINKCTL_LINK_BANDWIDTH_MGMT_IRQ_EN_G(pcieLinkControl)) {
		printf("%s    Link bandwidth management interrupt enabled\n", prefix);
	}
	if(PCI_EXP_LINKCTL_LINK_AUTONOMOUS_BANDWIDTH_IRQ_EN_G(pcieLinkControl)) {
		printf("%s    Link autonomous bandwidth interrupt enabled\n", prefix);
	}

	uint32 pcieLinkStatus = pciDevice->ReadConfigWord(pcieCap->CapOffset + PCI_EXP_LINKSTATUS);
	printf("%sPCIe link status: 0x%04X\n", prefix, (unsigned)pcieLinkStatus);
	unsigned linkSpeed = PCI_EXP_LNKSTA_CURRENT_LINK_SPEED_G(pcieLinkStatus);
	const char *linkSpeedStr = getLinkSpeedString(linkSpeed);
	printf("%s    Current link speed: %s", prefix, linkSpeedStr);
	printf("%s    Negotiated link width: %u\n", prefix, 
		(unsigned)PCI_EXP_LINKSTA_NEGOTIATED_LINK_WIDTH_G(pcieLinkStatus));
	if(PCI_EXP_LINKSTA_LINK_TRAINING_G(pcieLinkStatus)) {
		printf("%s    Link training\n", prefix);
	}
	if(PCI_EXP_LINKSTA_SLOT_CLOCK_CONFIG_G(pcieLinkStatus)) {
		printf("%s    Slot clock config set\n", prefix);
	}
	if(PCI_EXP_LINKSTA_DATA_LINK_LAYER_LINK_ACTIVE_G(pcieLinkStatus)) {
		printf("%s    Data link layer link active\n", prefix);
	}
	if(PCI_EXP_LINKSTA_LINK_BANDWIDTH_MANAGEMENT_STATUS_G(pcieLinkStatus)) {
		printf("%s    Link bandwidth management status set\n", prefix);
	}
	if(PCI_EXP_LINKSTA_LINK_AUTONOMOUS_BANDWIDTH_STATUS_G(pcieLinkStatus)) {
		printf("%s    Link autonomous bandwidth status set\n", prefix);
	}
	
	// ##### FIXME! ###### Remaining registers
}
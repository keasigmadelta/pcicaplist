/** PCIRegs.h
 *
 * PCI configuration registers (in addition to those defined in the AmigaOS headers.
 *
 * @Copyright (C) 2021 by Kea Signa Delta Limited, all rights reserved.
 *
 * See LICENSE file for licensing information.
 */

#pragma once


#define PCI_CONFIG_SPACE_SIZE 256
#define PCI_CONFIG_SPACE_EXT_SIZE 4096


// ----- PCI Express Capability Registers -----

#define PCI_EXP_FLAGS_REGOFFSET 2 // capability flags
#define    PCI_EXP_FLAGS_VERSION_G(x) ((x) & 0xF)
#define    PCI_EXP_FLAGS_TYPE_G(x) (((x) & 0xF0) >> 4)
#define        PCI_EXP_TYPE_PCIE_ENDPOINT 0x0 
#define        PCI_EXP_TYPE_LEGACY_ENDPOINT 0x1 
#define        PCI_EXP_TYPE_ROOT_PORT 0x4 // Root port
#define        PCI_EXP_TYPE_UPSTREAM_PORT 0x5
#define        PCI_EXP_TYPE_DOWNSTRAM_PORT 0x6
#define        PCI_EXP_TYPE_PCI_BRIDGE 0x7 // PCI/PCI-X bridge
#define        PCI_EXP_TYPE_ROOT_COMPLEX_INTEGRATED_ENDPOINT 0x9
#define        PCI_EXP_TYPE_ROOT_COMPLEX_EVENT_COLLECTOR 0x10
#define    PCI_EXP_FLAGS_SLOT_IMPLEMENTED_G(x) (((x) & 0x0100) >> 8)
#define    PCI_EXP_FLAGS_IRQ_MESSAGE_NUMBER_G(x) (((x) & 0x3e00) >> 9)

#define PCI_EXP_DEVCAP 4 // Device capabilities
#define    PCI_EXP_DEVCAP_MAX_PAYLOAD_SIZE_G(x) ((x) & 0x7)
#define    PCI_EXP_DEVCAP_PHANTOM_FUNCS_G(x) (((x) & 0x18) >> 3)
#define    PCI_EXP_DEVCAP_EXTENDED_TAGS_G(x) (((x) & 0x20) >> 5)
#define    PCI_EXP_DEVCAP_L0S_LATENCY_G(x) (((x) & 0x1c0) >> 6) // Acceptable L0s latency
#define    PCI_EXP_DEVCAP_L1_LATENCY_G(x) (((x) & 0xe00) >> 9) // Acceptable L1 latency
#define    PCI_EXP_DEVCAP_ATTENTION_BUTTON_G(x) (((x) & 0x1000) >> 12) // Attention button present
#define    PCI_EXP_DEVCAP_ATTENTION_INDICATOR_G(x) (((x) & 0x2000) >> 13) // Attention indicator present
#define    PCI_EXP_DEVCAP_POWER_INDICATOR_G(x) (((x) & 0x4000) >> 14) // Power indicator present
#define    PCI_EXP_DEVCAP_ROLE_BASED_ERROR_REPORTING_G(x) (((x) & 0x8000) >> 15)
#define    PCI_EXP_DEVCAP_POWER_VALUE_G(x) (((x) & 0x3fc0000) >> 18) // Slot power limit value
#define    PCI_EXP_DEVCAP_POWER_SCALE_G(x) (((x) & 0xC000000) >> 26) // Slot power limit scale
#define    PCI_EXP_DEVCAP_FUNCTION_LEVEL_RESET_G(x) (((x) & 0x10000000) >> 28)

#define PCI_EXP_DEVCTL 8 // Device control
#define    PCI_EXP_DEVCTL_CER_EN_G(x) ((x) & 0x1) // Enable correctable error reporting 
#define    PCI_EXP_DEVCTL_NFER_EN_G(x) (((x) & 0x2) >> 1) // Enable non-fatal error reporting
#define    PCI_EXP_DEVCTL_FER_EN_G(x) (((x) & 0x4) >> 2) // Enable fatal error reporting
#define    PCI_EXP_DEVCTL_URR_EN_G(x) (((x) & 0x8) >> 3) // Enable unsupported request reporting
#define    PCI_EXP_DEVCTL_RELAXED_ORDERING_EN_G(x) (((x) & 0x10) >> 4) // Enable relaxed ordering
#define    PCI_EXP_DEVCTL_MAX_PAYLOAD_SIZE_G(x) (((x) & 0xe0) >> 5) // Max. payload size
#define    PCI_EXP_DEVCTL_EXTENDED_TAG_EN_G(x) (((x) & 0x100) >> 8) // Extended tag field enable
#define    PCI_EXP_DEVCTL_PHANTOM_EN_G(x) (((x) & 0x200) >> 9) // Phantom functions enable
#define    PCI_EXP_DEVCTL_AUX_PM_EN_G(x) (((x) & 0x400) >> 10) // Auxiliary power PM enable
#define    PCI_EXP_DEVCTL_NOSNOOP_EN_G(x) (((x) & 0x800) >> 11) 
#define    PCI_EXP_DEVCTL_MAX_READ_REQ_SIZE_G(x) (((x) & 0x7000) >> 12)
#define    PCI_EXP_DEVCTL_BRIDGE_CONF_RETRY_FLR_G(x) (((x) & 0x8000) >> 15)

#define PCI_EXP_DEVSTATUS 10 // Device status
#define    PCI_EXP_DEVSTA_CORRECTABLE_ERROR_DETECTED_G(x) ((x) & 0x1)
#define    PCI_EXP_DEVSTA_NONFATAL_ERROR_DEECTED_G(x) (((x) & 0x2) >> 1)
#define    PCI_EXP_DEVSTA_FATAL_ERROR_DETECTED_G(x) (((x) & 0x4) >> 2)
#define    PCI_EXP_DEVSTA_UNSUPPORTED_REQ_DETECTED_G(x) (((x) & 0x8) >> 3)
#define    PCI_EXP_DEVSTA_AUX_POWER_DETECTED_G(x) (((x) & 0x10) >> 4)
#define    PCI_EXP_DEVSTA_TRANSACTIONS_PENDING_G(x) (((x) & 0x20) >> 5)

#define PCI_EXP_LINKCAP 12
#define    PCI_EXP_LNKCAP_SUPPORTED_LINK_SPEEDS_G(x) ((x) & 0xF)
#define        PCI_EXP_LINKCAP_SLS_UNKNOWN 0 // Unknown link speed
#define        PCI_EXP_LINKCAP_SLS_2_5GT 1 // 2.5GT/s
#define        PCI_EXP_LINKCAP_SLS_5_0GT 2
#define        PCI_EXP_LINKCAP_SLS_8_0GT 3
#define        PCI_EXP_LINKCAP_SLS_16_0GT 4
#define        PCI_EXP_LINKCAP_SLS_32_0GT 5
#define        PCI_EXP_LINKCAP_SLS_64_0GT 6
#define     PCI_EXP_LNKCAP_MAX_LINK_WIDTH_G(x) (((x) & 0x3f0) >> 4)
#define     PCI_EXP_LNKCAP_ASPMS_G(x) (((x) & 0xc00) >> 10) // Active State Power Management
#define     PCI_EXP_LNKCAP_L0S_EXIT_LATENCY_G(x) (((x) & 0x7000) >> 12)
#define     PCI_EXP_LNKCAP_L1_EXIT_LATENCY_G(x) (((x) & 0x38000) >> 15)
#define     PCI_EXP_LNKCAP_L1_CLOCK_PM_G(x) (((x) & 0x40000) >> 18)
#define     PCI_EXP_LNKCAP_SDERC_G(x) (((x) & 0x80000) >> 19) // Surprise Down Error Reporting Capable
#define     PCI_EXP_LNKCAP_DLLLARC_G(x) (((x) & 0x100000) >> 20) // Data link layer link active reporting capable
#define     PCI_EXP_LNKCAP_LBNC_G(x) (((x) & 0x200000) >> 21) // Link bandwidth notification capability
#define     PCI_EXP_LNKCAP_BIT22_G(x) (((x) & 0x400000) >> 22)
#define     PCI_EXP_LNKCAP_BIT23_G(x) (((x) & 0x800000) >> 23)
#define     PCI_EXP_LNKCAP_PORT_NUMBER_G(x) (((x) & 0xFF000000) >> 24) 

#define PCI_EXP_LINKCTL 16
#define     PCI_EXP_LINKCTL_ASPM_CONTROL_G(x) ((x) & 0x3)
#define     PCI_EXP_LINKCTL_READ_COMPLETION_BOUNDARY_G(x) (((x) & 0x8) >> 3)
#define     PCI_EXP_LINKCTL_LINK_DISABLE_G(x) (((x) & 0x10) >> 4)
#define     PCI_EXP_LINKCTL_RETRAIN_LINK_G(x) (((x) & 0x20) >> 5)
#define     PCI_EXP_LINKCTL_COMMON_CLOCK_CONFIG_G(x) (((x) & 0x40) >> 6)
#define     PCI_EXP_LINKCTL_EXTENDED_SYNC_G(x) (((x) & 0x80) >> 7)
#define     PCI_EXP_LINKCTL_EN_CLKREQ_G(x) (((x) & 0x100) >> 8)
#define     PCI_EXP_LINKCTL_HW_AUTONOMOUS_WIDTH_DISABLE_G(x) (((x) & 0x200) >> 9)
#define     PCI_EXP_LINKCTL_LINK_BANDWIDTH_MGMT_IRQ_EN_G(x) (((x) & 0x400) >> 10)
#define     PCI_EXP_LINKCTL_LINK_AUTONOMOUS_BANDWIDTH_IRQ_EN_G(x) (((x) & 0x800) >> 11)

#define PCI_EXP_LINKSTATUS 18
#define     PCI_EXP_LNKSTA_CURRENT_LINK_SPEED_G(x) ((x) & 0xF)
#define        PCI_EXP_LNKSTA_CLS_2_5GT 1 // NOTE: Matches PCI_EXP_LINKCAP_SLS_2_5GT, & co.
#define        PCI_EXP_LNKSTA_CLS_5_0GT 2
#define        PCI_EXP_LINKSTA_CLS_8_0GT 3
#define        PCI_EXP_LINKSTA_CLS_16_0GT 4
#define        PCI_EXP_LINKSTA_CLS_32_0GT 5
#define        PCI_EXP_LINKSTA_CLS_64_0GT 6
#define     PCI_EXP_LINKSTA_NEGOTIATED_LINK_WIDTH_G(x) (((x) & 0x3F0) >> 4)
#define     PCI_EXP_LINKSTA_LINK_TRAINING_G(x) (((x) & 0x800) >> 11)
#define     PCI_EXP_LINKSTA_SLOT_CLOCK_CONFIG_G(x) (((x) & 0x1000) >> 12)
#define     PCI_EXP_LINKSTA_DATA_LINK_LAYER_LINK_ACTIVE_G(x) (((x) & 0x2000) >> 13)
#define     PCI_EXP_LINKSTA_LINK_BANDWIDTH_MANAGEMENT_STATUS_G(x) (((x) & 0x4000) >> 14)
#define     PCI_EXP_LINKSTA_LINK_AUTONOMOUS_BANDWIDTH_STATUS_G(x) (((x) & 0x8000) >> 15)


// ----- PCI-Express Extended capabilities -----

#define PCI_EXT_CAPABILITYID_ERR 0x01 // Advanced error reporting
#define PCI_EXT_CAPABILITYID_VC 0x02 // Virtual channel 
#define PCI_EXT_CAPABILITYID_DSN 0x03 // Device serial number
#define PCI_EXT_CAPABILITYID_PWR 0x04 // Power budgeting
#define PCI_EXT_CAPABILITYID_RCLD 0x05 // Root complex link declaration
#define PCI_EXT_CAPABILITYID_RCILC 0x06 // Root complex internal link control
#define PCI_EXT_CAPABILITYID_RCEC 0x07 // Root complex event collector
#define PCI_EXT_CAPABILITYID_MFVC 0x08 // Multi-function VC
#define PCI_EXT_CAPABILITYID_VC9 0x09 // Virtual channel (alt. version)
#define PCI_EXT_CAPABILITYID_RCRB 0x0A // Root complex RB?
#define PCI_EXT_CAPABILITYID_VENDOR 0x0B // Vendor specific 
#define PCI_EXT_CAPABILITYID_CAC 0x0C // Config access (obsolete)
#define PCI_EXT_CAPABILITYID_ACS 0x0D // Access control services
#define PCI_EXT_CAPABILITYID_ARI 0x0E // Alternative routing ID
#define PCI_EXT_CAPABILITYID_ATS 0x0F // Address translation services
#define PCI_EXT_CAPABILITYID_SRIOV 0x10 // Single root I/O virtualization
#define PCI_EXT_CAPABILITYID_MRIOV 0x11 // Multi root I/O virtualization
#define PCI_EXT_CAPABILITYID_MCAST 0x12 // Multicast
#define PCI_EXT_CAPABILITYID_PRI 0x13 // Page request interface
#define PCI_EXT_CAPABILITYID_AMD_XXX 0x14 // Reserved for AMD
#define PCI_EXT_CAPABILITYID_REBAR 0x15 // Resizable BAR
#define PCI_EXT_CAPABILITYID_DPA 0x16 // Dynamic power allocation
#define PCI_EXT_CAPABILITYID_TPH 0x17 // TPH requester
#define PCI_EXT_CAPABILITYID_LTR 0x18 // Latency tolerance reporting
#define PCI_EXT_CAPABILITYID_SECPCI 0x19 // Secondary PCIe capability
#define PCI_EXT_CAPABILITYID_PMUX 0x1A // Protocol multiplexing
#define PCI_EXT_CAPABILITYID_PASID 0x1B // Process address space ID
#define PCI_EXT_CAPABILITYID_DPC 0x1D // Downstream port containment
#define PCI_EXT_CAPABILITYID_L1SS 0x1E // L1 PM substates
#define PCI_EXT_CAPABILITYID_PTM 0x1F // Precision time measurement
#define PCI_EXT_CAPABILITYID_DLF 0x25 // Data link feature
#define PCI_EXT_CAPABILITYID_PL_16GT 0x26 // Physical layer 16.0 GT/s

#define PCI_EXT_CAPABILITYID_MAX PCI_EXT_CAPABILITYID__16GT

/** PrintPCICapabilities.h
 *
 * Functions to print information from PCI capabilities.
 *
 * @author Hans de Ruiter
 *
 * @Copyright (C) 2021 by Kea Sigma Delta Limited, all rights reserved.
 *
 * See LICENSE file for licensing information.
 */

#pragma once


#include <expansion/expansion.h>
#include <expansion/pci.h>

#include <proto/expansion.h>

/** Prints the information for the PCI-Express capability registers.
 *
 * @param pciDevice the PCI device
 * @param pcieCap pointer to the PCI-Express capability
 * @param prefix a string to add before every line (e.g., "\t")
 */
void printPCIExpressCapRegs(struct PCIDevice *pciDevice, struct PCICapability *pcieCap, const char *prefix);